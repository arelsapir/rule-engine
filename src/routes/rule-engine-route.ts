import * as express from 'express'
import { dbConnectionAsync, getFacts, getRules } from '../lib/sql-server'

export const ruleEngineRouterAsync = (async () => {
    const dbQuery = await dbConnectionAsync()
    const router = express.Router()

    router.get('/facts', async (req, res, next) => {
        const tableName = req.query['tableName'] as string
        if (tableName) {
            const ans = await getFacts(tableName, dbQuery)
            res.send(ans)
        } else { res.sendStatus(422) } // missing mandatory params
    })

    router.get('/rules', async (req, res, next) => {
        const tableName = req.query['tableName'] as string
        if (tableName) {
            const ans = await getRules(tableName, dbQuery)
            res.send(ans)
        } else { res.sendStatus(422) } // missing mandatory params
    })

    return router
})