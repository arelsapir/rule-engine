import * as express from 'express'
import { ruleEngineRouterAsync } from './rule-engine-route'

export const mainRouterAsync = (async () => {
    const ruleEngineRouter = await ruleEngineRouterAsync()
    const router = express.Router()

    router.get('/healthz', async (req, res, next) => {
        try {
            res.sendStatus(200)
        } catch(e) {
            res.send(e)
        }
    })

    // router.use('/api', ruleEngineRouter)
    router.use(ruleEngineRouter)
    return router
})