import * as cors from 'cors'
import { cpus } from 'os'
import * as express from 'express'
import { port } from './env'
import { mainRouterAsync } from './routes/main-router'
import { Cluster } from 'cluster'
import { logger } from './lib/logger'
const cluster: Cluster = require('cluster')
const server = express()
const numberOfCPUs = cpus().length

export async function init() {
    if (cluster.isMaster) {
        logger(ConsoleLayerEnum.log, 0, `API Server cluster (process id ${process.pid}) is running on port ${port}`)
        // Fork workers.
        for (let i = 0; i < numberOfCPUs; i++) {
            cluster.fork();
        }
        cluster.on('exit', (worker, code, signal) => {
            logger(ConsoleLayerEnum.log, 0, `worker (process id ${worker.process.pid}) died`);
        });
    } else {
        const mainRouter = await mainRouterAsync()
        // Workers can share any TCP connection
        // In this case it is an HTTP server
        server.use(cors({
            origin: true,
            credentials: true
        }))
        server.use(express.json())
        server.use(express.urlencoded({ extended: false }))
        server.use(mainRouter)

        server.listen(port, () => logger(ConsoleLayerEnum.log, 0, `Worker process (process id ${process.pid}) started`) )
    }
}