import { DISABLE_VERBOSE, MAX_VERBOSE_LEVEL, MAX_CONSOLE_LAYER } from '../env'

const consoleLayerPriority: Record<ConsoleLayer, number> = { // the lower the console level is the more important it is
    [ConsoleLayerEnum.error] : 0,
    [ConsoleLayerEnum.warn]: 1,
    [ConsoleLayerEnum.log]: 2,
}

const maxConsolePriority = consoleLayerPriority[MAX_CONSOLE_LAYER]

// The logger only shows logs if both the console layer priority and the verbose priority meet the requirement of the env variables. logger shows ALL logs by default
export const logger = (consoleLayer: ConsoleLayer, verboseLevel: VerboseLevel, ...logs: string[]) => {
    if (DISABLE_VERBOSE) { return }
    if ((consoleLayerPriority[consoleLayer] <= maxConsolePriority) && 
        (verboseLevel <= MAX_VERBOSE_LEVEL)) { // the lower the verbose level is the more important it is
            console[consoleLayer](...logs)
    }
}