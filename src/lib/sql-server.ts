import { AZURE_STORAGE_CONNECTION_STRING } from '../env'
import * as sql from 'mssql'
import { ruleConfigurations } from '../core/rule-configurations'
import { logger } from './logger'
import { hasPrimaryKeyQuery, primaryKeyCountColumnsQuery,
    numberOfRowsQuery, numberOfIndexesQuery } from '../core/db-queries'

export const dbConnectionAsync = (async () => {
    try {
        const connection = await sql.connect(`${AZURE_STORAGE_CONNECTION_STRING}Encrypt=true`) // using Encrypt=true since DB is hosted on Azure
        return connection.query
    } catch (e) {
        logger(ConsoleLayerEnum.error, 0, `failed connecting to DB, check connection string environment variable. ${e}`)
    }
})

export const sqlResponseParser = (response: sql.IResult<any>): any | null => {
    try {
        return Object.keys(response.output).length ? 
        response.output :
        response.recordset[0]['']
    } catch (e) {
        logger(ConsoleLayerEnum.error, 0, `failed parsing response from DB. ${e}`)
        return null
    }
}

export const getFacts = ( async ( table: string, query: (command: string) => Promise<sql.IResult<any>> ): Promise<FactsResponse> => {
    logger(ConsoleLayerEnum.log, 3, `Querying the count of rows for ${table}`)
    const numberOfRows = sqlResponseParser( await query(numberOfRowsQuery(table)) )
    logger(ConsoleLayerEnum.log, 2, `Successfully queried the count of rows for ${table}`)

    logger(ConsoleLayerEnum.log, 3, `Querying the count of indexes for ${table}`)
    const numberOfIndexes = sqlResponseParser( await query(numberOfIndexesQuery(table)) )
    logger(ConsoleLayerEnum.log, 2, `Successfully queried the count of indexes for ${table}`)

    logger(ConsoleLayerEnum.log, 3, `Querying if primary key exists for ${table}`)
    const hasPrimaryKey = sqlResponseParser( await query(hasPrimaryKeyQuery(table)) ) === 'true' ? true : false
    logger(ConsoleLayerEnum.log, 2, `Successfully queried if primary key exists for ${table}`)

    hasPrimaryKey && logger(ConsoleLayerEnum.log, 3, `Querying the count of primary key columns for ${table}`)
    const primaryKeyCountColumns = hasPrimaryKey && sqlResponseParser( await query(primaryKeyCountColumnsQuery(table)) )
    logger(ConsoleLayerEnum.log, 2, `Successfully queried the count of primary key columns for ${table}`)

    logger(ConsoleLayerEnum.log, 1, `Successfully calculated facts for ${table}`)
    
    return {
        "table-name": table,
        "number-of-rows": numberOfRows,
        "number-of-indexes": numberOfIndexes,
        "has-primary-key": hasPrimaryKey,
        ...(hasPrimaryKey ? {"primary-key-count-columns": primaryKeyCountColumns} : {})
    }
})

export const getRules = ( async ( table: string, query: (command: string) => Promise<sql.IResult<any>> ): Promise<RuleResponse[]> => {
    const tableFacts = await getFacts(table, query)
    const rules: RuleResponse[] = []
    for (const ruleDefinition of ruleConfigurations) {
        try {
            logger(ConsoleLayerEnum.log, 3, `Calculating new rule`)
            const newRule = ruleDefinition(tableFacts)
            logger(ConsoleLayerEnum.log, 2, `Successfully calculated rule of ${newRule.ruleName}`)
            rules.push(newRule)
        } catch (e) {
            logger(ConsoleLayerEnum.warn, 0, `failed to process rule ${e}`)
        }
    }
    logger(ConsoleLayerEnum.log, 1, `Successfully calculated rules for ${table}`)
    return rules
})