export const port = process.env.PORT || 4000
export const AZURE_STORAGE_CONNECTION_STRING = process.env.AZURE_STORAGE_CONNECTION_STRING || "Server=metisdb1.database.windows.net;Database=ORM;User Id=user1;Password=Trustno1;" // used string explicitly for sake of simplicity
export const DISABLE_VERBOSE = process.env.DISABLE_VERBOSE === 'true'
export const MAX_VERBOSE_LEVEL = ( process.env.MAX_VERBOSE_LEVEL && parseInt(process.env.MAX_VERBOSE_LEVEL) ) || 3
export const MAX_CONSOLE_LAYER: ConsoleLayer = (process.env.MAX_CONSOLE_LAYER || 'log') as ConsoleLayer