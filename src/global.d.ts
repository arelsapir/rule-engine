declare interface FactsResponse {
    "table-name": string
    "number-of-rows": number
    "number-of-indexes": number
    "has-primary-key": boolean
    "primary-key-count-columns"?: number | false
}

declare interface RuleResponse {
    ruleName: string
    status: string
    message: string
}

declare interface RuleConfiguration {
    (tableFacts: FactsResponse): RuleResponse
}

declare const enum ConsoleLayerEnum {
    'error' = 'error',
    'warn' = 'warn',
    'log' = 'log',
}

declare type ConsoleLayer = ConsoleLayerEnum.log | ConsoleLayerEnum.warn | ConsoleLayerEnum.error

declare type MIN_CONSOLE_LEVEL = ConsoleLayer

declare type VerboseLevel = 0 | 1 | 2 | 3 // the lower the verbose level is the more important it is