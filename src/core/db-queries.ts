export const numberOfRowsQuery = (table: string) => `
    SELECT count(*)
    FROM ${table}
`
export const numberOfIndexesQuery = (table: string) => `
    SELECT count(*) 
    FROM  sys.indexes AS IND
    WHERE object_id = object_ID('${table}')
    AND index_id != 0
`
export const hasPrimaryKeyQuery = (table: string) => `
    SELECT CASE
        WHEN Count(index_id) = 1 THEN 'true'
    	ELSE 'false'
    	END
    FROM sys.indexes
    WHERE object_id = object_id('${table}')
    AND is_primary_key = 1
`
export const primaryKeyCountColumnsQuery = (table: string) => `
    SELECT COUNT(INC.column_id)
    FROM sys.indexes as IND
		INNER JOIN sys.index_columns as INC
			ON IND.object_id = INC.object_id
			AND IND.index_id = INC.index_id
    WHERE IND.object_id = object_id('${table}')
	    AND IND.is_primary_key = 1
`