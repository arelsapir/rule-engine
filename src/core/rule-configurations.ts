const RULE_SUCCESS_RESPONSE = 'Pass'
const RULE_FAILURE_RESPONSE = 'Failed'
const HIGH_NUMBER_OF_ROWS = 10000000

export const ruleConfigurations: RuleConfiguration[] = [

    (tableFacts: FactsResponse): RuleResponse => {
        let rulePassed = true
        const numberOfRows = tableFacts["number-of-rows"]
        if (numberOfRows > HIGH_NUMBER_OF_ROWS) { rulePassed = false }

        return {
            ruleName: "High number of rows",
            message: rulePassed ?
                `The table has a small number of rows (${numberOfRows})` :
                `Warning! Large table. The number of rows is ${numberOfRows}`,
            status: rulePassed ? RULE_SUCCESS_RESPONSE : RULE_FAILURE_RESPONSE,
        }
    },
    
    (tableFacts: FactsResponse): RuleResponse => {
        let rulePassed = true
        const hasPrimaryKey = tableFacts["has-primary-key"]
        if (!hasPrimaryKey) { rulePassed = false }

        return {
            ruleName: "No Primary Key",
            message: rulePassed ?
                `The table has a Primary Key` :
                `Warning: the table doesn’t have a PK`,
            status: rulePassed ? RULE_SUCCESS_RESPONSE : RULE_FAILURE_RESPONSE,
        }
    },
    
    (tableFacts: FactsResponse): RuleResponse => {
        let rulePassed = true
        const primaryKeyColumns = tableFacts["primary-key-count-columns"]
        if (primaryKeyColumns > 3) { rulePassed = false }

        return {
            ruleName: "a Primary Key with many columns",
            message: rulePassed ?
                `Number of columns in PK is ok (${primaryKeyColumns})` :
                `High number of columns in the PK (${primaryKeyColumns})`,
            status: rulePassed ? RULE_SUCCESS_RESPONSE : RULE_FAILURE_RESPONSE,
        }
    }
]