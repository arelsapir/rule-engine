import * as dotenv from "dotenv"
import { init } from './server'

dotenv.config() // reads environment variables from a file named .env at the root directory
init() // exposing the API