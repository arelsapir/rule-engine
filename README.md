# Metis - Interview Challenge

## Getting Started

Rule Engine for SQL Server databases.

### Prerequisites

[docker-compose](https://docs.docker.com/compose/install/) has to be installed on your machine.

### Installing

simply run in the command line from within the project's folder

```
docker-compose up --build
```

### API testing - Client Side

use an HTTP agent (postman, etc) or in any web browser of your choice
go to http://localhost:4000/facts?tableName=TABLE_NAME in the browser or a GET request in the agent.
for example http://localhost:4000/facts?tableName=courses
the response can also be shown in the browser's DevTools.

## Description

Once up and running all endpoints will be listening on port 4000 by default.
for example use the browser HTTP GET request by browsing to http://localhost:4000/healthz to see you are up and running.

Multiple NodeJS worker processes are listening to incoming requests, using a round-robin implementation, there is no session related logic following the REST api principles.
Allowing consistency regardless the chosen worker process intercepting the request.

## API testing - Backend Side

### Logs
the logger class implementation in the code comes with a vast priority range for logging.

using an *.env* file that for the sake of efficiency is included in the project, multiple environment variables are added for verbosity controlling of the server logs.

#### logs filtering by Verbose levels
by default we show all logs, which is max verbose level 3.
changing the MAX_VERBOSE_LEVEL in the *.env* file to any number from 0 to 2 will show less logs.
the lower the number the more important the log is.

#### logs filtering by console layers
by default we show all logs, which is max console layer 'log'
possible console layers are 'error' and 'warn'
the priority is such that 'error' is more important than 'warn' and 'warn' is more important than 'log'
changing the MAX_CONSOLE_LAYER in the *.env* file to 'error' or 'warn' will show less logs.
for example. 'error' shows only erros, 'warn' shows both errors and warnings, and 'log' shows all. 

#### Note
log filtering is depending on both max verbose level and max console layer.

## License

This project was created by Arel Sapir, licensed under the MIT License.